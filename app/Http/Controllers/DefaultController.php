<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Manufacturer;
use App\Product;

class DefaultController extends Controller
{
    public function today()
    {
        $date = new \DateTime();
    	return view('name', [
    		'name' => $date->format('Y-m-d'),
			'slogan' => 'Siandien yra:'  
		]);
    }

    public function name($name='3WA')
    {
    $firstLetter = mb_strtoupper(mb_substr($name, 0, 1));
    $name = $firstLetter . mb_substr($name, 1);
    return view('name', [
    	'name'=> $name,
    	'slogan'=> 'mano vardas'
    	]);
 	}
    
    
    public function categories(){
		
		$categories = Category::all();
		return view('categories', ['list'=> $categories]);
	
	}

	public function manufacturers(){
		
		$manufacturers = Manufacturer::all();
		return view('manufacturers', ['list'=> $manufacturers]);
	
	}

	public function products(){
		
		$products = Product::all();
		return view('products', ['items'=> $products]);
	
	}
}



