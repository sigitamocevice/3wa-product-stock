<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductRelationShips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            //pakeicia stulpelio info. Reiksme gales buti tik >0
            $table->integer('quantity')->unsigned()->change();
            
            //sukuria naujus stulpeliis
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('manufacturer_id')->unsigned()->nullable();

            //Foreign key susieja irasus tarpusavyje
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers');
        
        });
    }

       

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
