<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelationSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
           
            //sukuria naujus stulpeliis
            $table->integer('supplier_id')->unsigned()->nullable();
            //Foreign key susieja irasus tarpusavyje
            $table->foreign('supplier_id')->references('id')->on('suppliers');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
