-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 13, 2017 at 08:32 
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `3wa_product_stock`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'SEDAN', NULL, NULL),
(3, 'HECHBECK', NULL, NULL),
(5, 'ROADSTER', NULL, NULL),
(7, 'CROSSOVER', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE `manufacturers` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `title`, `website_url`, `country`, `created_at`, `updated_at`) VALUES
(1, 'FIAT', 'www.fiat.lt', 'IT', NULL, NULL),
(2, 'ALFA ROMEO', 'www.alfaromeo.it', 'IT', NULL, NULL),
(3, 'MASERATI', '', 'IT', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2017_03_08_133547_create_manufacturers_table', 1),
(9, '2017_03_08_140021_create_products_table', 1),
(10, '2017_03_08_140048_create_categories_table', 1),
(11, '2017_03_09_085131_ProductRelationShips', 2),
(12, '2017_03_09_135711_ProductImageUrl', 3),
(13, '2017_03_09_141432_ProductImageColumnDelete', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `manufacturer_id` int(10) UNSIGNED DEFAULT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `price`, `quantity`, `created_at`, `updated_at`, `category_id`, `manufacturer_id`, `image_url`) VALUES
(1, 'FIAT TIPO123', 'Copmact class', '9999.99', 10, NULL, '2017-03-10 10:52:35', 1, 1, 'https://i.ytimg.com/vi/yf3e7SES4FA/maxresdefault.jpg'),
(2, 'FIAT 500', 'Urban class', '8900.99', 20, NULL, NULL, 3, 1, 'https://www.fiat.com/content/dam/fiat/cross/models/500_family/family_page/990/canvas/500.jpg.transform/width-990/img.jpg'),
(3, 'AR GIULIA', 'Luxury class', '9999.99', 2, NULL, NULL, 1, 2, '\r\n\r\nhttps://www.alfaromeo.ca/content/alfaca/en/cars/alfa-romeo-giulia/_jcr_content/image.img.png/1464031795521.png'),
(4, 'AR 4C', 'Sport class\r\n\r\n', '9099.99', 1, NULL, NULL, 5, 2, 'https://s-media-cache-ak0.pinimg.com/originals/57/b4/12/57b4120334fe0a2081c880b4bc8d73d9.jpg'),
(5, 'Maserati Quattroporte', 'LUXURY SportSedan', '9999.00', 3, NULL, NULL, 1, 3, 'http://www.maserati.com/mediaObject/sites/international/Models/Quattroporte/In-Page/2016-05/FRONTALE_v1/resolutions/res-870x582/FRONTALE_v1.jpg'),
(6, 'Maserati Levante', 'LUXURY CROSSOVER', '9999.00', 3, NULL, NULL, 7, 3, 'http://blog.caranddriver.com/wp-content/uploads/2016/06/Screen-Shot-2016-06-01-at-10.36.41-PM.png'),
(12, 'dg', 'dg', '2.00', 2, '2017-03-10 07:49:58', '2017-03-10 07:49:58', 1, 1, 'http://images.car.bauercdn.com/upload/29787/images/maserati03.jpg'),
(15, 'HKHK', 'HKJH', '5.00', 5, '2017-03-10 08:17:40', '2017-03-10 08:17:40', 1, 1, 'http://images.car.bauercdn.com/upload/29787/images/maserati03.jpg'),
(16, 'Levante', 'sport', '9999.00', 1, '2017-03-10 09:12:33', '2017-03-10 09:12:33', 1, 1, 'http://st.automobilemag.com/uploads/sites/11/2016/03/Maserati-Levante-front-three-quarte-in-motion-03.jpg'),
(17, 'FIAT 500', 'Urban class', '8900.99', 20, '2017-03-10 10:22:22', '2017-03-10 10:22:22', 1, 1, 'https://www.fiat.com/content/dam/fiat/cross/models/500_family/family_page/990/canvas/500.jpg.transform/width-990/img.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`),
  ADD KEY `products_manufacturer_id_foreign` (`manufacturer_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `products_manufacturer_id_foreign` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
