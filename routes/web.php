<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/







Route::get('/', 'ProductController@index');


Route::resource('products', 'ProductController');
Route::resource('categories', 'CategoryController');
Route::resource('manufacturers', 'ManufacturerController');
Route::resource('suppliers', 'SupplierController');


Auth::routes();

Route::get('/home', 'HomeController@index');
