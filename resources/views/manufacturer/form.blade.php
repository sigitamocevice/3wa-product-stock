@extends('layouts.app')


	@section('content')
		
		<h2>GAMINTOJU keitimas</h2>

		@if(isset($manufacturer))
			{!! Form::model($manufacturer, [
			'route' => ['manufacturers.update', $manufacturer->id],
			'method' => 'put'
			]) !!}
			@else
				{!! Form::open(['route' => 'manufacturers.store', 'method' => 'post']) !!}
			@endif
						

			<div class="from-group">    
			{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title'])!!}
			</div>
			<div class="from-group">    
			{!! Form::text('website_url', null, ['class' => 'form-control', 'placeholder' => 'Title'])!!}
			</div>
			<div class="from-group">    
			{!! Form::text('country', null, ['class' => 'form-control', 'placeholder' => 'Title'])!!}
			</div>
			
			

			{!! Form::submit('Save' ,['class' => 'btn-primary']) !!}
			

		{!! Form::close() !!}



		@if(isset($manufacturer))
			{!! Form::open([
			'route' => ['manufacturers.destroy', $manufacturer->id],
			'method' => 'delete' 
			])
			 !!}
				{!! Form::submit('Delete' , ['class' => 'btn btn-danger'])!!}
			 
			{!! Form::close() !!}
		@endif

@endsection