@extends('layouts.app')

	@section('content')
	
	<div class="container">
		<div class="row">
		<h2>GAMINTOJU SARASAS</h2>
			<div class="col-md-6 col-md-offset-3">
				<a class="btn btn-success" href="{{ route('manufacturers.create') }}">SUKURTI NAUJA GAMINTOJA</a>
				<hr>
					<ul>
    	   			 @foreach ($items as $manufacturer) 
		    		    <li>
					    	<a href="{{route('manufacturers.show',['id'=>$manufacturer->id])}}">{{ $manufacturer->title }}</a>
					    </li>
					    <li>
					    	<a class="btn btn-primary marginB2" href="{{ route('manufacturers.edit', $manufacturer->id) }}">koreguoti</a>
					    </li>
					    <hr>
		    		@endforeach
					</ul>
			</div>
		</div>
	</div>
		
		
	@endsection