@extends('layouts.app')

	@section('content')

	<a href="#" class="list-group-item active"><h1>Galimi gamintojai:</h1>
    
  </a>
	<div class="list-group">

  <a href="#" class="list-group-item">{{ $manufacturer->title }}</a>
	
 		 
 		 @foreach ($manufacturer->products as $product) 
		<div class="col-sm-6 col-md-4">
			<div class="thumbnail">
				<img src="{{ $product->image_url}} ">
					<div class="caption">
						
						<p> 
							
							@if ($product->manufacturer)
							<strong> GAMINTOJAS: {{ $product->manufacturer->title}} </strong>
							@endif 
						 </p>
						 <p>
						 	<a href="{{route('manufacturers.edit', $manufacturer->id) }}" class="btn btn-default" role="button" class="btn btn-primary" role="button">PERZIURETI</a> <a href="">Button</a>
						 </p>
					</div>			
			</div>
		</div>
		 @endforeach	

	</div>

@endsection


