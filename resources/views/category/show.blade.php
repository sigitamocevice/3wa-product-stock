@extends('layouts.app')

	@section('content')

	<a href="#" class="list-group-item active"><h1>Galimos kategorijos:</h1>
    
  </a>
	<div class="list-group">

  <a href="#" class="list-group-item">{{ $category->title }}</a>
	
 		 
 		 @foreach ($category->products as $product) 
		<div class="col-sm-6 col-md-4">
			<div class="thumbnail">
				<img src="{{ $product->image_url}} ">
					<div class="caption">
						
						<p> 
							@if ($product->category)
							<strong> KATEGORIJA: {{ $product->category->title}} </strong>
							@endif
							
						 </p>
						 
						 	 <p>
						 	<a href="{{route('categories.edit', $category->id) }}" class="btn btn-primary">EDIT</a>
						 </p>
						 	
					</div>			
			</div>
		</div>
		 @endforeach	

	</div>

@endsection




