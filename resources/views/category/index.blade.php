	@extends('layouts.app')

	@section('content')
	
	<div class="container">
		<div class="row">
		<h2>KATEGORIJU SARASAS</h2>
			<div class="col-md-6 col-md-offset-3">
				<a class="btn btn-success" href="{{ route('categories.create') }}">SUKURTI NAUJA KATEGORIJA</a>
				<hr>
					<ul>
    	   			 @foreach ($items as $category) 
		    		    <li>
					    	<a href="{{route('categories.show',['id'=>$category->id])}}">{{ $category->title }}</a>
					    </li>
					    <li>
					    	<a class="btn btn-primary marginB2" href="{{ route('categories.edit', $category->id) }}">koreguoti</a>
					    </li>
					    <hr>
		    		@endforeach
					</ul>
			</div>
		</div>
	</div>
		
		
	@endsection


