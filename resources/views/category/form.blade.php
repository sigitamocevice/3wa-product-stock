@extends('layouts.app')


	@section('content')
		
		<h2>KATEGORIJOS keitimas</h2>

		@if(isset($category))
			{!! Form::model($category, [
			'route' => ['categories.update', $category->id],
			'method' => 'put'
			]) !!}
			@else
				{!! Form::open(['route' => 'categories.store', 'method' => 'post']) !!}
			@endif
						

			<div class="from-group">    
			{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title'])!!}
			</div>
			
			

			{!! Form::submit('Save' ,['class' => 'btn-primary']) !!}
			

		{!! Form::close() !!}



		@if(isset($category))
			{!! Form::open([
			'route' => ['categories.destroy', $category->id],
			'method' => 'delete' 
			])
			 !!}
				{!! Form::submit('Delete' , ['class' => 'btn btn-danger'])!!}
			 
			{!! Form::close() !!}
		@endif

@endsection