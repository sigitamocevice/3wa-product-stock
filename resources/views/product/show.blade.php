@extends('layouts.app')

	@section('content')

	<h2>{{ $product->title }}</h2>
	<img src="{{ $product->image_url}}" class="img-responsive">

	<p>{{ $product->description }}</p>
	<ul>
		<li>{{ $product->price }}</li>
		<li>{{ $product->quantity }}</li>
		<li>{{ $product->category->title }}</li>
		<li>{{ $product->manufacturer->title }}</li>


	</ul>


		@if (Auth::check()) {
   		 <a href="{{route('products.edit', $product->id) }}" class="btn btn-primary">EDIT</a>
		}
		@endif 
	@endsection
