@extends('layouts.app')


	@section('content')
		
		<h2>Produkto sukurimas</h2>

		@if(isset($product))
			{!! Form::model($product, [
			'route' => ['products.update', $product->id],
			'method' => 'put',
			'files' => true
			]) !!}
		@else
				{!! Form::open(['route' => 'products.store', 'method' => 'post', 'files' => true]) !!}
		@endif
			<div class="from-group">    
			{!! Form::select('category_id', $categories, null, ['class' => 'form-control'])!!}
			</div>

			<div class="from-group">    
			{!! Form::select('manufacturer_id', $manufacturers, null, ['class' => 'form-control'])!!}
			</div>

			<div class="from-group">    
			{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title'])!!}
			</div>
			
			<div class="from-group">  
			{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
			</div>
			
			
						
			<div class="from-group">  
			{!! Form::number('price', null, ['class' => 'form-control', 'placeholder' => 'Price']) !!}
			</div>
			
			<div class="from-group">  
			{!! Form::number('quantity', null, ['class' => 'form-control', 'placeholder' => 'Quantity']) !!}
			</div>

			<div class="from-group">  
			{!! Form::file('image_url', null, ['class' => 'form-control', 'placeholder' => 'IMAGE']) !!}

			</div>
	@if (Auth::check()) {

			{!! Form::submit('Save' ,['class' => 'btn-primary']) !!}
	}
		@endif 	

		{!! Form::close() !!}



		@if(isset($product))
			{!! Form::open([
			'route' => ['products.destroy', $product->id],
			'method' => 'delete' 
			])
			 !!}
				{!! Form::submit('Delete' , ['class' => 'btn btn-danger'])!!}
			 
			{!! Form::close() !!}
		@endif

@endsection