@extends('layouts.app')

	@section('content')

	<h2>Automobiliu sarasas</h2>

	<div class="row">
		@foreach ($items as $product) 
		<div class="col-sm-6 col-md-4">
			<div class="thumbnail">
				 <img src="{{ asset('storage/' . $product->image_url) }}" class="img-fluid"  >
					<div class="caption">
						<h3> {{ $product->title }} </h3>
						<p> {{ $product->description }} </p>
						<p>
							<strong>KAINA:</strong>
							{{ $product->price }} 
							<strong>EUR</strong>
							<br>
							<strong>KIEKIS:</strong>
							{{ $product->quantity }} 
							<strong>vnt.</strong>
							
						</p>
						<p> 
							@if ($product->category)
							<strong> KATEGORIJA: {{ $product->category->title}} </strong>
							@endif
							<br>
							@if ($product->manufacturer)
							<strong> GAMINTOJAS: {{ $product->manufacturer->title}} </strong>
							@endif 
							<br>
							@if ($product->supplier)
							<strong> TIEKEJAS: {{ $product->supplier->title}} </strong>
							@endif 
						 </p>
						 <p>
						 	<a href="{{route('products.show', $product->id) }}" class="btn btn-primary" role="button">PERZIURETI</a> 
						 </p>
					</div>			
			</div>
		</div>
		 @endforeach	
	</div>
	@endsection


