@extends('layouts.app')

@section('content')
    <p>{{ $slogan }}: <strong>{{ $name }}</strong></p>
@endsection

@section('sidebar')
	@parent
	<p>KAIP TAU EINAS?</p>
@endsection