@extends('layouts.app')

	@section('content')
	
	<div class="container">
		<div class="row">
		<h2>TIEKEJU SARASAS</h2>
			<div class="col-md-6 col-md-offset-3">
				<a class="btn btn-success" href="{{ route('suppliers.create') }}">SUKURTI NAUJA TIEKEJA</a>
				<hr>
					<ul>
    	   			 @foreach ($items as $supplier) 
		    		    <li>
					    	<a href="{{route('suppliers.show',['id'=>$supplier->id])}}">{{ $supplier->title }}</a>
					    </li>
					    <li>
					    	<a class="btn btn-primary marginB2" href="{{ route('suppliers.edit', $supplier->id) }}">koreguoti</a>
					    </li>
					    <hr>
		    		@endforeach
					</ul>
			</div>
		</div>
	</div>
		
		
	@endsection