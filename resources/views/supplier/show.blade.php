@extends('layouts.app')
@section('content')
<h2>Supplier: {{ $supplier->title }}</h2>
<a href="{{route('suppliers.edit', $supplier->id) }}" class="btn btn-warning marginB">Edit supplier</a>
<div class="row">
    @foreach ($supplier->products as $product) 
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img src="{{$product->image_url}}" alt="{{ $product->title }}">
            <div class="caption">
                <h3>{{ $product->title }}</h3>
                <p>{{ $product->description }}</p>
                <p>
                    Kaina: {{ $product->price }}. 
                    Kiekis: {{ $product->quantity }}.
                </p>
                <p>
                    @if ($product->category)
                    <strong>Kategorija: </strong>{{ $product->category->title }}
                    @endif
                    
                    @if ($product->manufacturer)
                    <br>
                    <strong>Gamintojas: </strong>{{ $product->manufacturer->title }}
                    @endif
                    @if ($product->supplier)
                    <br>
                    <strong>Supplier: </strong>{{ $product->supplier->title }}
                    @endif
                </p>
                <a class="btn btn-primary" href="{{ route('products.show', ['id' => $product->id]) }}">Peržiūrėti</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection