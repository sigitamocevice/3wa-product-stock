@extends('layouts.app')
@section('content')
@if(isset($supplier))
<h2>Supplier redagavimas:</h2>
{{-- editinimo forma --}}
{!! Form::model($supplier,['route' => ['suppliers.update',$supplier->id], 'method' => 'put']) !!}
@else
<h2>Supplier sukūrimas:</h2>
{{-- creat forma --}}
{!! Form::open(['route' => 'suppliers.store', 'method' => 'post']) !!}
@endif
<div class="form-group">
    {!!Form::label('title', 'Title')!!}
    {!!Form::text('title', null, ['class' => 'form-control','placeholder'=>'Title']) !!}
</div>
{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
{{-- delete forma --}}
@if(isset($supplier))
{!! Form::open(['route' => ['suppliers.destroy', $supplier->id], 'method' => 'delete']) !!}
{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
{!! Form::close() !!}
@endif
@endsection