@extends('layouts.app')

	@section('content')

	<h2>Gamintoju sarasas:</h2>
	<ul>
		@foreach ($list as $manufacturer) 
			<li>{{ $manufacturer->title }}</li>
	    @endforeach	
	</ul>
	
@endsection